
import Foundation

public struct SearchResultsDateJSON: Decodable {
    public let text: String
    public let year: Int?
    public let number: Int?
    public let found: Bool?
    public let type: String?
}

public enum SearchResult {
    case Success(SearchResultsDateJSON)
    case Error(String)
}
